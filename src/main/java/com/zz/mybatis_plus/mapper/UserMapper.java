package com.zz.mybatis_plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zz.mybatis_plus.pojo.User;
import org.springframework.stereotype.Repository;

/**
 * @author zz
 * @description TODO
 * @createTime 2021/8/7 20:30
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}

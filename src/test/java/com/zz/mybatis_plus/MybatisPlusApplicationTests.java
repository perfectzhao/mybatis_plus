package com.zz.mybatis_plus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zz.mybatis_plus.mapper.UserMapper;
import com.zz.mybatis_plus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MybatisPlusApplicationTests {

    @Autowired
    private UserMapper userMapper;
    @Test
    void contextLoads() {
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

    @Test
    public void instert(){
        User user = new User();
        user.setName("zz2");
        user.setAge(18);
        int insert = userMapper.insert(user);
        System.out.println(insert);
    }
    // 乐观锁成功情况
    @Test
    public void testOptimisticLocker(){
        User user = userMapper.selectById(1);
        user.setName("xiaobai");
        userMapper.updateById(user);

    }
    // 乐观锁失败情况
    @Test
    public void testOptimisticLocker2(){
        User user = userMapper.selectById(1L);
        user.setName("xiaoming");
        // 模拟线程插入
        User user1 = userMapper.selectById(1L);
        user1.setName("xiaohuang");
        userMapper.updateById(user1);
        userMapper.updateById(user);
    }

    @Test
    public void testPage(){
        Page<User> pages = new Page<>(1,5);
        userMapper.selectPage(pages, null);
        pages.getRecords().forEach(System.out::println);
        System.out.println(pages.getTotal());
    }
    @Test
    public void testDelete(){
        int delete = userMapper.deleteById(1L);
        System.out.println(delete);
    }



}

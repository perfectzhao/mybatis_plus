package com.zz.mybatis_plus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zz.mybatis_plus.mapper.UserMapper;
import com.zz.mybatis_plus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @author zz
 * @description 条件构造器的简单使用
 * @createTime 2021/8/8 19:27
 */
@SpringBootTest
public class QueryWrapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    void test1(){
        // 查询name不为空的用户，并且邮箱不为空的用户，年龄大于等于20
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.isNotNull("name")
                .isNotNull("email")
                .ge("age",20);
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }
    //查询一个数据，出现多个结果使用List或者 Map
    @Test
    void test2(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name","zz");
//        User user = userMapper.selectOne(queryWrapper);
//        System.out.println(user);
        List<User> users = userMapper.selectList(queryWrapper);
        users.forEach(System.out::println);
        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        maps.forEach(System.out::println);
    }
    @Test
    void test3(){
        // 查询年龄在 20 ~ 30 岁之间的用户
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.between("age",20,30);
        Integer count = userMapper.selectCount(wrapper);
        System.out.println(count);

    }
    @Test
    void test4(){
        // 模糊查询
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.notLike("name","e")
                .likeRight("email","t");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void test5(){
        // 通过id进行排序
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }
}
